#!/usr/local/bin/python3

import argparse
import sqlite3

from datetime import datetime, timedelta

# Number of records to be inserted into database per INSERT transaction.
INSERT_COUNT_PER_DB_TRANSACTION = 100


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input_file', required=True, help="Input file (CSV Format)")
    parser.add_argument(
        '-o', '--output_file', required=True, help="Output file (SQLite 3)")
    args = parser.parse_args()

    connection = sqlite3.connect(args.output_file)
    cursor = connection.cursor()

    file_handler = open(args.input_file, "r")
    row_count = 0
    data = []
    proceed = True

    while True:
        if row_count == 0:
            # Skip as it's a header row.
            file_handler.readline()
            row_count += 1
            continue

        line = file_handler.readline()
        if line:
            row_count += 1
            id, timestamp_start, temperature, duration = \
                line.strip("\n").split(",")
            timestamp_start = datetime.strptime(
                timestamp_start, '%Y-%m-%d %H:%M:%S.%f')
            days, _, _extra = duration.split(" ")
            hours, minutes, seconds = _extra.split(":")
            time_gap = timedelta(
                days=int(days),
                hours=int(hours),
                minutes=int(minutes),
                seconds=float(seconds)
            )
            timestamp_end = timestamp_start + time_gap
            data.append([
                id, timestamp_start, temperature, timestamp_end,
                str(datetime.now()), str(datetime.now())
            ])
        else:
            proceed = False
        if row_count % INSERT_COUNT_PER_DB_TRANSACTION == 0 or not proceed:
            cursor.executemany(
                """INSERT INTO events_event (id, timestamp_start, temperature,
                timestamp_end, timestamp_created, timestamp_updated
                ) VALUES (?, ?, ?, ?, ?, ?);""", data)
            data = []
            if not proceed:
                break

    file_handler.close()
    connection.commit()
    connection.close()
