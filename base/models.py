from django.db import models


class BaseModel(models.Model):
    """
    An abstract class from which model's can be derived so as to include any
    common fields; say creation (and updation) timestamps.
    """

    timestamp_created = models.DateTimeField(
        auto_now_add=True
    )

    timestamp_updated = models.DateTimeField(
        auto_now=True
    )

    class Meta:
        abstract = True
