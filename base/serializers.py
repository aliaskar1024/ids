from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


class PaginatedSerializer:
    """
    A serializer that performs pagination of given data as well.
    queryset: Denotes the django queryset.
            type: <queryset>
    page: Denotes the page number
        type: <int>
    num: Denotes the number of records per page.
        type: <int>
    serializer: Denotes the model serializer corresponding to given `queryset`.
        type: <serializers.ModelSerializer>
    :return: type: <dict>
        `total` represents the total number of records (int)
        `previous` represents the previous page (int)
        `next` represents the previous page (int)
        `result` represents the data for the page (OrderedDict)
    """

    def __init__(
            self, queryset, page, num, serializer=None, context=None,
            **kwargs):

        paginator = Paginator(queryset, num)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)
        count = paginator.count
        previous = None if not objects.has_previous() else \
            objects.previous_page_number()
        next = None if not objects.has_next() else \
            objects.next_page_number()

        if serializer:
            serializer = serializer(
                objects.object_list, many=True, context=context, **kwargs)
            data = serializer.data
        else:
            data = objects.object_list

        self.pages_count = paginator.num_pages
        self.data = {
            'total': count,
            'previous': previous,
            'next': next,
            'result': data
        }
