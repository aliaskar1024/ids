import json
import copy
from functools import wraps
from django.contrib.contenttypes.models import ContentType

from logs.models import HTTPRequestLog


def log_http_request(func):

    @wraps(func)
    def func_wrapper(*args, **kwargs):
        request = args[1]
        META = getattr(request, 'META', {})
        response = func(*args, **kwargs)
        http_request_log = HTTPRequestLog(
            request_path=request.get_full_path(),
            request_method=request.method,
            request_params=get_request_details(request),
            remote_addr=META.get('REMOTE_ADDR', '-'),
            server_protocol=META.get('SERVER_PROTOCOL', '-'),
            http_user_agent=META.get('HTTP_USER_AGENT', '-'),
            response_payload=response.data,
            response_status_code=response.status_code
        )

        # Save the user details (if performed after authentication)
        if request and request.user and hasattr(request.user, '_meta'):
            http_request_log.request_user_type = ContentType.objects.get(
                app_label=request.user._meta.app_label,
                model=request.user.__class__.__name__.lower())
            http_request_log.request_user_id = request.user.id

        http_request_log.save()
        return response

    def get_request_details(request):
        request_details = {}
        if request.POST:
            request_details = dict(request.POST)
        elif request.GET:
            request_details = dict(request.GET)
        elif request.body:
            try:
                request_body_obj = json.loads(request.body.decode('utf-8'))
                request_details = copy.deepcopy(request_body_obj)
            except ValueError:
                pass
        return request_details

    return func_wrapper


