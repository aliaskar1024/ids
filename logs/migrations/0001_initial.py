# Generated by Django 3.0.2 on 2020-01-20 18:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='HTTPRequestLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp_created', models.DateTimeField(auto_now_add=True)),
                ('timestamp_updated', models.DateTimeField(auto_now=True)),
                ('request_path', models.TextField()),
                ('request_method', models.CharField(choices=[('GET', 'GET'), ('HEAD', 'HEAD'), ('POST', 'POST'), ('PUT', 'PUT'), ('DELETE', 'DELETE'), ('CONNECT', 'CONNECT'), ('OPTIONS', 'OPTIONS'), ('TRACE', 'TRACE'), ('PATCH', 'PATCH')], max_length=7)),
                ('request_params', models.TextField()),
                ('remote_addr', models.CharField(max_length=255)),
                ('server_protocol', models.CharField(max_length=255)),
                ('http_user_agent', models.TextField()),
                ('response_payload', models.TextField()),
                ('response_status_code', models.IntegerField(null=True)),
                ('request_user_id', models.PositiveIntegerField(null=True)),
                ('request_user_type', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='content_type_timelines', to='contenttypes.ContentType')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
