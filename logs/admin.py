from django.contrib import admin

from logs.models import HTTPRequestLog


@admin.register(HTTPRequestLog)
class HTTPRequestLogAdmin(admin.ModelAdmin):

    list_display = (
        'id', 'request_path', 'request_method', 'request_user',
        'get_response_status_code', 'get_response_time'
    )

    def get_response_status_code(self, log):
        return str(log.response_status_code)

    def get_response_time(self, log):
        return str(
            (log.timestamp_updated - log.timestamp_created).microseconds
        )

    get_response_time.short_description = "Response Time (ms)"
