from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from base.models import BaseModel
from logs.constants import (
    GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH)


class HTTPRequestLog(BaseModel):

    HTTP_METHODS = (
        (GET, 'GET'),
        (HEAD, 'HEAD'),
        (POST, 'POST'),
        (PUT, 'PUT'),
        (DELETE, 'DELETE'),
        (CONNECT, 'CONNECT'),
        (OPTIONS, 'OPTIONS'),
        (TRACE, 'TRACE'),
        (PATCH, 'PATCH'),

    )

    request_path = models.TextField()

    request_method = models.CharField(choices=HTTP_METHODS, max_length=7)

    request_params = models.TextField()

    remote_addr = models.CharField(max_length=255)

    server_protocol = models.CharField(max_length=255)

    http_user_agent = models.TextField()

    response_payload = models.TextField()

    response_status_code = models.IntegerField(null=True)

    request_user_type = models.ForeignKey(
        ContentType,
        related_name="content_type_timelines",
        on_delete=models.CASCADE,
        null=True
    )
    request_user_id = models.PositiveIntegerField(null=True)

    request_user = GenericForeignKey('request_user_type', 'request_user_id')

    @property
    def timestamp_start(self):
        return self.timestamp_created

    @property
    def timestamp_end(self):
        return self.timestamp_updated
