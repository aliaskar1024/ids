# IDS

A prototype to expose events over REST API using Python/ Django.

###### **API Endpoints**
**1. List Events Api**

    Endpoint: GET /api/events
    Request Parameters (Optional): 
        1. count: An integer that denotes number of records fetched in per request.
            Example: GET /api/events?count=10
        2. page: An integer that denotes page number.
            Example: GET /api/events?page=2
                     GET /api/events?page=2&count=10
        3. ordering: Denotes the field by which the order should be maintained.
            Example: GET /api/events?ordering=timestamp_start
                     GET /api/events?ordering=-timestamp_start
                     GET /api/events?ordering=timestamp_end
                     GET /api/events?ordering=-timestamp_end
         
    Sample Response: https://api.myjson.com/bins/ro2ga
    
    

###### **Other features:**
1. Package includes a python application `loader.py` to load data from csv to the SQLite3 database.
2. Every Events API hit is logged in the database. Below are the parameters that are being logged:  
    - requested URL
    - requested user
    - http request method
    - request parameters
    - remote address
    - http user agent
    - response payload
    - response status code

Logs are visible at below URL for admins.
    
        GET /admin/logs/httprequestlog/

For admin registration, refer the _Setup Procedure_ section.

###### **Dependencies:**
- python 3.7.3
- django 3.0.2
- django rest framework 3.11.0

###### **Setup Procedure:**
1. Setup virtualenv and virtualenvwrapper - These are tools that support isolated python environments. Instructions can be found at : https://help.dreamhost.com/hc/en-us/articles/115000695551-Installing-and-using-virtualenv-with-Python-3
This step is preferred but optional. Once activated, follow below instructions.
2. Install the dependencies for the project using below command:
    
        pip3 install -r requirements.txt

3. Run database migrations - this step creates required tables in the database.

        python3 manage.py migrate

4. Create a admin user using below command (optional). Required to login to admin and see request logs. 
Enter a username, email and password as prompted.

        python3 manage.py createsuperuser

5. Run the tests and ensure we are on track. All tests should pass without errors.

        python3 manage.py test

6. Load the csv data into database using below command.

        python3 loader.py --input_file <input_csv_file_path> --output_file <sqlite3_db_file_path>
        
    Example:
    
        python3 loader.py --input_file /tmp/task_data.csv --output_file db.sqlite3

7. Run the server using below command to make it accessible at http://localhost:8000:

        python3 manage.py runserver


###### **Q & A**
1. Why was Django used ?

    Django is one the popular web framework built on python that supports mostly all required features for developing a web application. Another alternative considered was Flask. These are frameworks that helps to spin up a web server very easily. The expertise with Django over Flask is one reason it was chosen here.
        
2. Why was SQLite3 used ?

    SQLite3 is generally recommended for smaller applications, prototypes, demos etc. Also, the complexity in shipping it is minimal compared to other databases. But, it is not recommended on a production environment.
    
3. Why was Django Rest Framework used ?
        
    DRF is a very flexible toolkit for building API endpoints.

4. Why was API made instead of a basic page exposing the data ?
        
    The purpose of having API is many; to support multiple platforms, reliability, scalability etc.

5. Why was there a need for pagination ?

    If data like the one given is to be shown to user, it could have been a sort of visualization may be. Let's say plotting on a graph. As there could be sevral thousands of events, the purpose was to lazy load the data as user scrolls.

6. Why was the decision on logging made using a decorator ?

     Initially i thought of having a middleware that logs every HTTP request. It logs every HTTP request; admin, static files etc. This could have been handled but the scope here was to focus on specific API I belive. If it was that every HTTP request to the app is to be logged, a middleware better serves the purpose.
         
7. Is the `EventHelper` service required ? Why was it written so ?

    The idea of having the EventHelper was to have all the logic of Event moved there for reusability, and to avoid business logics from views. As of now, this is not clearly visible as it EventHelper doesn't do much at this moment.
 
 