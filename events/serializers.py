from rest_framework import serializers

from events.models import Event


class EventSerializer(serializers.ModelSerializer):
    """
    Serializer for `Event` model.
    """

    HUMAN_READABLE_FORMAT = \
        "{days} days {hours}:{minutes}:{seconds}.{microseconds}"

    timestamp_start = serializers.SerializerMethodField(
        read_only=True
    )

    duration = serializers.SerializerMethodField(
        read_only=True
    )

    class Meta:
        model = Event
        fields = ['id', 'timestamp_start', 'duration', 'temperature']

    @staticmethod
    def get_timestamp_start(event):
        return event.timestamp_start

    @staticmethod
    def get_duration(event):
        time_delta = event.duration
        return EventSerializer.HUMAN_READABLE_FORMAT.format(
            days=time_delta.days,
            hours="{:02d}".format(time_delta.seconds // 3600),
            minutes=time_delta.seconds % 3600 // 60,
            seconds=time_delta.seconds % 60,
            microseconds=time_delta.microseconds
        )
