from events.models import Event


class EventHelper(object):

    DEFAULT_ORDERING = 'timestamp_start'

    CUSTOM_ORDERING_FIELDS = ["timestamp_start", "timestamp_end"]

    @staticmethod
    def fetch(ordering=None):
        """
        Returns the events satisfying the criteria.
        :param ordering: Indicates the ordering of queryset.
            Return type: str
        :return: `queryset` of events.
        """

        if ordering and \
                ordering.strip("-") not in EventHelper.CUSTOM_ORDERING_FIELDS:
            assert "Invalid value for `ordering`."

        ordering = EventHelper.DEFAULT_ORDERING if not ordering else ordering

        return Event.objects.order_by(ordering)
