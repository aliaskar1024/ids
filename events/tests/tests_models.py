from datetime import datetime, timedelta


from events.models import Event
from events.tests import EventTest


class EventModelTest(EventTest):

    def test_event_creation(self):
        assert Event.objects.count() == 2

    def test_event_data(self):

        format = '%Y-%m-%d %H:%M:%S.%f'

        for event_dict in self.event_data:
            timestamp_start = datetime.strptime(
                event_dict['timestamp_start'], '%Y-%m-%d %H:%M:%S.%f')
            days, _, _extra = event_dict['duration'].split(" ")
            hours, minutes, seconds = _extra.split(":")
            time_gap = timedelta(
                days=int(days),
                hours=int(hours),
                minutes=int(minutes),
                seconds=float(seconds)
            )
            timestamp_end = timestamp_start + time_gap

            event = Event.objects.get(id=event_dict['id'])

            assert event.timestamp_start.strftime(
                format) == timestamp_start.strftime(format)

            assert event.timestamp_end.strftime(
                format) == timestamp_end.strftime(format)

            assert event.temperature == event_dict['temperature']

            assert event.id == event_dict['id']
