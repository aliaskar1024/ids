from events.helper import EventHelper
from events.tests import EventTest


class EventHelperTest(EventTest):

    def test_helper(self):
        events = EventHelper.fetch()
        assert events.count() == 2

    def test_helper_response(self):
        event_1_id = self.event_data[0]['id']
        event_2_id = self.event_data[1]['id']

        events = EventHelper.fetch()
        assert events[0].id == event_1_id
        assert events[1].id == event_2_id

        events = EventHelper.fetch(ordering='id')
        assert events[0].id == event_1_id
        assert events[1].id == event_2_id

        events = EventHelper.fetch(ordering='-id')
        assert events[0].id == event_2_id
        assert events[1].id == event_1_id
