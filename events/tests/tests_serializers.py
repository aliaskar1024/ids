from rest_framework.serializers import ListSerializer
from rest_framework.utils.serializer_helpers import ReturnList

from events.models import Event
from events.serializers import EventSerializer
from events.tests import EventTest


class EventSerializerTest(EventTest):

    fields = ['id', 'timestamp_start', 'duration', 'temperature']

    def test_serializer(self):
        events = Event.objects.all()
        serializer = EventSerializer(events, many=True)
        assert type(serializer) == ListSerializer
        assert type(serializer.data) == ReturnList
        assert set(serializer.data[0].keys()) - set(self.fields) == set()
        assert set(serializer.data[1].keys()) - set(self.fields) == set()
