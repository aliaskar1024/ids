from events.tests import EventTest


class EventViewTest(EventTest):

    def test_api_response(self):
        response = self.client.get('/api/events')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(hasattr(response, 'json'))

        response_json = response.json()
        assert response_json['total'] == 2
        assert 'result' in response_json.keys()
        assert response_json['result']
        assert response_json['result'][0]['id'] == self.event_data[0]['id']
        assert response_json['result'][1]['id'] == self.event_data[1]['id']

    def test_api_pagination(self):
        response = self.client.get('/api/events')
        response_json = response.json()
        assert not response_json['previous']
        assert not response_json['next']

        response = self.client.get('/api/events?count=1&page=1')
        response_json = response.json()
        assert not response_json['previous']
        assert response_json['total'] == 2
        assert response_json['next'] == 2
        assert len(response_json['result']) == 1
        assert response_json['result'][0]['id'] == self.event_data[0]['id']

        response = self.client.get('/api/events?count=1&page=2')
        response_json = response.json()
        assert response_json['total'] == 2
        assert response_json['previous'] == 1
        assert not response_json['next']
        assert len(response_json['result']) == 1
        assert response_json['result'][0]['id'] == self.event_data[1]['id']

    def test_api_ordering(self):
        response = self.client.get('/api/events?ordering=id')
        response_json = response.json()
        assert response_json['result'][0]['id'] == self.event_data[0]['id']

        response = self.client.get('/api/events?ordering=-id')
        response_json = response.json()
        assert response_json['result'][0]['id'] == self.event_data[1]['id']
