from django.test import TestCase
from datetime import datetime, timedelta

from events.models import Event


class EventTest(TestCase):

    def setUp(self):

        self.event_data = [
            {
                'id': 37432,
                'timestamp_start': '2019-06-05 23:09:13.407799',
                'duration': '0 days 00:31:06.294035753',
                'temperature': '1547.9101687142627'
            },
            {
                'id': 37450,
                'timestamp_start': '2019-06-05 23:09:31.407799',
                'duration': '0 days 00:30:28.174694816',
                'temperature': '1526.4769084430313'
            }
        ]

        for event_dict in self.event_data:
            timestamp_start = datetime.strptime(
                event_dict['timestamp_start'], '%Y-%m-%d %H:%M:%S.%f')
            days, _, _extra = event_dict['duration'].split(" ")
            hours, minutes, seconds = _extra.split(":")
            time_gap = timedelta(
                days=int(days),
                hours=int(hours),
                minutes=int(minutes),
                seconds=float(seconds)
            )
            timestamp_end = timestamp_start + time_gap
            Event.objects.create(
                id=event_dict['id'],
                timestamp_start=timestamp_start,
                timestamp_end=timestamp_end,
                temperature=event_dict['temperature']
            )
