from django.db import models

from base.models import BaseModel


class Event(BaseModel):
    """
    Model that represent an event.
    """

    timestamp_start = models.DateTimeField()

    timestamp_end = models.DateTimeField()

    temperature = models.CharField(max_length=18)

    @property
    def duration(self):
        """
        Returns the duration of event in `TimeDelta` format.
        :return: TimeDelta
        """

        return self.timestamp_end - self.timestamp_start

    def __str__(self):
        return str(self.id)
