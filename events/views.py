from django.conf import settings
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status as http_status


from base.serializers import PaginatedSerializer
from events.helper import EventHelper
from events.serializers import EventSerializer
from logs.decorators import log_http_request


API_PAGE_COUNT_DEFAULT = getattr(settings, 'API_PAGE_COUNT_DEFAULT', 25)
API_PAGE_COUNT_MAX = getattr(settings, 'API_PAGE_COUNT_MAX', 25)


class ListEventsAPIView(APIView):

    serializer_class = EventSerializer

    @log_http_request
    def get(self, request):

        count = int(request.GET.get('count', API_PAGE_COUNT_DEFAULT))
        page = int(request.GET.get('page', 1))
        ordering = request.GET.get('ordering', None)

        events = EventHelper.fetch(ordering)

        serialized_data = PaginatedSerializer(
            events,
            page,
            min(count, API_PAGE_COUNT_MAX),
            self.serializer_class
        )

        return Response(serialized_data.data, http_status.HTTP_200_OK)
