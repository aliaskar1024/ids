from django.urls import path

from . import views

urlpatterns = [

    # List all events (supports pagination).
    path('', views.ListEventsAPIView.as_view(), name='list-events'),

]
